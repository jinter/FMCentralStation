
// FMReview.h : PROJECT_NAME 应用程序的主头文件
//

#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

#include "resource.h"		// 主符号


// CFMReviewApp:
// 有关此类的实现，请参阅 FMReview.cpp
//
class CLogFile;
class CPrintFrame;
class CFMReviewDlg;
class CFMReviewApp : public CWinApp
{
public:
	CFMReviewApp();

// 重写
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// 实现
	DECLARE_MESSAGE_MAP()

private:
	CLogFile* m_pLogFile;
	CFMReviewDlg* m_pMainDlg;
	BOOL m_bAutoDiagno;
	CCriticalSection m_csLogLock;

public:
	BOOL m_bTendPrintHR;
	BOOL m_bTendPrintSYS;
	BOOL m_bTendPrintMEA;
	BOOL m_bTendPrintDIA;
	BOOL m_bTendPrintRR;
	BOOL m_bTendPrintSPO2;
	BOOL m_bTendPrintT1;
	BOOL m_bTendPrintT2;
	CString m_sHospitalName;
	int m_nNIBPUnitOption;
	int m_nTEMPUnitOption;
	int m_nFHRReportType;
	int m_nDoctorDiagno;

	void WriteLog(CString s);
	inline BOOL HasAutoDiagno() { return m_bAutoDiagno; };
	int GetRecordFileList(CStringArray& sa,
		CUIntArray* paOffset=NULL, CUIntArray* paSize=NULL);
};

extern CFMReviewApp theApp;