#pragma once
#include "reviewdlg.h"
class CReviewTendTable : public CReviewTool
{
private:

	//����
	CFont m_fontNum;
	CFont* m_pOldFont;

public:
	CReviewTendTable(CReviewDlg* pdlg);
	virtual ~CReviewTendTable(void);

private:
	virtual int  GetPageSpan();
	virtual void EnterReview();
	virtual void LeaveReview();
	virtual void Draw(CDC* pdc);

	void DrawGrid(CDC* pdc, CRect& rc);
	void DrawColumnName(CDC* pdc, CRect& rc);
	void DrawData(CDC* pdc, CRect& rc);
	void DrawAlarmGrid(CDC* pdc, int x, int y);
};

