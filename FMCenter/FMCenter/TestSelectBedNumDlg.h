#pragma once
#include "afxwin.h"


// CTestSelectBedNumDlg 对话框
class CFMToolbarForm;
class CTestSelectBedNumDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CTestSelectBedNumDlg)

private:
	int m_nType;
	int m_nSelectedBedIndex;
	CFMToolbarForm* m_pParent;

public:
	CTestSelectBedNumDlg(CFMToolbarForm* pParent, int nType);
	virtual ~CTestSelectBedNumDlg();
	int GetSelectedBedIndex() { return m_nSelectedBedIndex; };

// 对话框数据
	enum { IDD = IDD_TEST_SELECT_BED_NUM };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCancel();
	CComboBox m_cmbBedNum;
	afx_msg void OnBnClickedOk();
	virtual BOOL OnInitDialog();
};
