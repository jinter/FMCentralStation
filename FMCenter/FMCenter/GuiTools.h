#ifndef __GUITOOLS_H__
#define __GUITOOLS_H__

//打印预览使用的回调函数声明
typedef void(*DRAWFUN)(CDC* pDC, CPrintInfo* pInfo, void* pVoid);

void DrawLine(CDC* pDC, COLORREF cr, int x, int y1, int y2);
void DrawLineAA(CDC* pDC, COLORREF cr, int x, int y1, int y2);
COLORREF MixColor(COLORREF cr1, COLORREF cr2, int scale1, int scale2);
void DrawECGWave(CDC* pdc, COLORREF cr, int x, int y, BYTE* data, int limitW, int limitH);
void DrawECGWave50(CDC* pdc, COLORREF cr, int x, int y, BYTE* data, int limitW, int limitH);
void DrawECGWave20(CDC* pdc, COLORREF cr, int x, int y, BYTE* data, int limitW, int limitH);
void DrawECGWave10(CDC* pdc, COLORREF cr, int x, int y, BYTE* data, int limitW, int limitH);
void DrawFMMark(CDC* pdc, COLORREF cr, int x, int y);
void DrawTOCOResetMark(CDC* pdc, COLORREF cr, int x, int y);
void DrawTimeMark(CDC* pdc, COLORREF cr, CTime& tm, int x, int y, int h);

#endif


