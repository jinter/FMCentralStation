#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "afxdtctl.h"


// CMedicalRecordListDlg 对话框

class CMedicalRecordListDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CMedicalRecordListDlg)

public:
	CMedicalRecordListDlg(CWnd* pParent, int nType);
	virtual ~CMedicalRecordListDlg();

// 对话框数据
	enum { IDD = IDD_MEDICAL_RECORD };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()

private:
	int m_nType;
	CDatabase& m_db;
	CCriticalSection& m_dbLock;
	int m_nRecordCount;
	int m_nPageStartIndex;
	int m_nListValidCount;
	int m_nListCurrentIndex;

	CString m_sSearchMedicalNum;
	CString m_sSearchPatientName;
	CString m_sSearchPatientType;
	CString m_sSearchGender;
	CString m_sSearchAddress;
	CString m_sSearchPhoneNumber;
	CString m_sSearchClinic;
	CString m_sSearchDoctor;
	CString m_sSearchAdmission;

	CString m_sSelectedMedicalNum;
public:
	CString GetSelectedMedicalNum() { return m_sSelectedMedicalNum; };

private:
	void ClearSearchString();
	void InitRecordCounter(CString& sWhereStatement);
	void MakeWhereStatement(CString& sWhereStatement);
	void MakeQueryString(CString& sql, CString& sWhereStatement);
	
public:
	void UpdateRecordList(BOOL bGotoLastPage = FALSE);
	void UpdateDetialPanel(CString& sMedicalNum);
	void IncreaseRecordCounter() { m_nRecordCount ++; };
	void DecreaseRecordCounter() { if (m_nRecordCount > 0) m_nRecordCount --; };
	void SetNaviButtonState();

public:
	CListCtrl m_lstRecord;
	CButton m_btnFirst;
	CButton m_btnPrev;
	CButton m_btnNext;
	CButton m_btnLast;
	CButton m_btnEdit;
	CButton m_btnNew;
	CButton m_btnDelete;
	CEdit m_edtMedicalNum;
	CEdit m_edtPatientName;
	CEdit m_edtPatientType;
	CEdit m_edtGender;
	CEdit m_edtBloodtype;
	CEdit m_edtHeight;
	CEdit m_edtWeight;
	CEdit m_edtBirthday;
	CEdit m_edtAddress;
	CEdit m_edtCreateDate;
	CEdit m_edtClinic;
	CEdit m_edtDoctor;
	CEdit m_edtPhoneNumber;
	CEdit m_edtPageInfo;
	CEdit m_edtAdminName;
	CEdit m_edtAdmissionDate;
	CButton m_btnSelect;
	CEdit m_edtSearchMedicalNum;
	CEdit m_edtSearchPatientName;
	CComboBox m_cmbSearchPatientType;
	CComboBox m_cmbSearchGender;
	CEdit m_edtSearchAddress;
	CEdit m_edtSearchPhoneNumber;
	CEdit m_edtSearchClinic;
	CEdit m_edtSearchDoctor;
	CDateTimeCtrl m_datSearchAdmission;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBtnFirst();
	afx_msg void OnBnClickedBtnPrev();
	afx_msg void OnBnClickedBtnNext();
	afx_msg void OnBnClickedBtnLast();
	afx_msg void OnBnClickedBtnEdit();
	afx_msg void OnBnClickedBtnNew();
	afx_msg void OnBnClickedBtnDelete();
	afx_msg void OnNMClickLstRecord(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDbClickLstRecord(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedBtnSelect();
	afx_msg void OnBnClickedBtnSearchByMediclNum();
	afx_msg void OnBnClickedBtnSearchByName();
	afx_msg void OnBnClickedBtnSearchByType();
	afx_msg void OnBnClickedBtnSearchByGender();
	afx_msg void OnBnClickedBtnSearchByAddress();
	afx_msg void OnBnClickedBtnSearchByPhoneNumber();
	afx_msg void OnBnClickedBtnSearchByClinic();
	afx_msg void OnBnClickedBtnSearchByDoctor();
	afx_msg void OnBnClickedBtnSearchByAdmission();
	afx_msg void OnBnClickedBtnSearchAll();
	afx_msg void OnBnClickedCancel();
};
