// MedicalRecordEditDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "FMCenter.h"
#include "MedicalRecordListDlg.h"
#include "MedicalRecordEditDlg.h"
#include "afxdialogex.h"


// CMedicalRecordEditDlg 对话框

IMPLEMENT_DYNAMIC(CMedicalRecordEditDlg, CDialogEx)

CMedicalRecordEditDlg::CMedicalRecordEditDlg(CMedicalRecordListDlg* pParent, int nType, CString sMedNum)
	: CDialogEx(CMedicalRecordEditDlg::IDD, pParent)
	, m_db(theApp.m_db)
	, m_dbLock(theApp.m_csDBLock)
	, m_sMedNum(sMedNum)
	, m_pListDlg(pParent)
	, m_nType(nType)
{
}

CMedicalRecordEditDlg::~CMedicalRecordEditDlg()
{
}

void CMedicalRecordEditDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_EDT_NAME, m_edtName);
	DDX_Control(pDX, IDC_EDIT_DATETIME_BIRTHDAY, m_dateBirthday);
	DDX_Control(pDX, IDC_EDIT_EDT_HEIGHT, m_edtHeight);
	DDX_Control(pDX, IDC_EDIT_EDT_WEIGHT, m_edtWeight);
	DDX_Control(pDX, IDC_EDIT_EDT_ADDRESS, m_edtAddress);
	DDX_Control(pDX, IDC_EDIT_CMB_SEX, m_cmbGender);
	DDX_Control(pDX, IDC_EDIT_CMB_TYPE, m_cmbType);
	DDX_Control(pDX, IDC_EDIT_CMB_BLOOD_TYPE, m_cmbBloodType);
	DDX_Control(pDX, IDC_EDIT_EDT_RECORD_NUM, m_edtRecordNum);
	DDX_Control(pDX, IDC_EDIT_EDT_DOCTOR, m_edtDoctor);
	DDX_Control(pDX, IDC_EDIT_EDT_CLINIC, m_edtClinic);
	DDX_Control(pDX, IDC_EDIT_PHONE_NUMBER, m_edtPhoneNumber);
	DDX_Control(pDX, IDC_DAT_ADMISSION_DATE, m_datAdmissionDate);
}


BEGIN_MESSAGE_MAP(CMedicalRecordEditDlg, CDialogEx)
	ON_BN_CLICKED(IDCANCEL, &CMedicalRecordEditDlg::OnBnClickedCancel)
	ON_BN_CLICKED(ID_SAVE, &CMedicalRecordEditDlg::OnBnClickedSave)
END_MESSAGE_MAP()


// CMedicalRecordEditDlg 消息处理程序


void CMedicalRecordEditDlg::OnBnClickedSave()
{
	CString sName;
	CString sDoctor;
	CString sHeight;
	CString sWeight;
	CString sGender;
	CString sBloodType;
	CString sPatientType;
	CString sBirthday;
	CString sClinic;
	CString sAdmissionDate;
	CString sCreateDate;
	CString sAddress;
	CString sPhoneNumber;
	CString sOperator;

	UpdateData(TRUE);

	//取得数值
	m_edtName.GetWindowText(sName);
	m_edtDoctor.GetWindowText(sDoctor);
	m_edtHeight.GetWindowText(sHeight);
	m_edtWeight.GetWindowText(sWeight);
	m_edtClinic.GetWindowText(sClinic);
	m_edtAddress.GetWindowText(sAddress);
	m_edtPhoneNumber.GetWindowText(sPhoneNumber);
	sOperator = theApp.m_sAdminName;
	sGender.Format(_T("%d"), m_cmbGender.GetCurSel());
	sBloodType.Format(_T("%d"), m_cmbBloodType.GetCurSel());
	sPatientType.Format(_T("%d"), m_cmbType.GetCurSel());
	CTime tm;
	m_dateBirthday.GetTime(tm);
	sBirthday = tm.Format(_T("%Y-%m-%d 0:0:0"));
	m_datAdmissionDate.GetTime(tm);
	sAdmissionDate = tm.Format(_T("%Y-%m-%d %H:%M:%S"));

	//检验数值
	if (!sHeight.IsEmpty() && !IsPositiveNum(sHeight)) {
		MessageBox(_T("身高数值不合法！"), _T("提示"), MB_ICONEXCLAMATION | MB_OK);
		m_edtHeight.SetFocus();
		return;
	}
	if (!sWeight.IsEmpty() && !IsPositiveNum(sWeight)) {
		MessageBox(_T("体重数值不合法！"), _T("提示"), MB_ICONEXCLAMATION | MB_OK);
		m_edtWeight.SetFocus();
		return;
	}

	//构造SQL字符串
	tm = CTime::GetCurrentTime();
	sCreateDate = tm.Format(_T("%Y-%m-%d %H:%M:%S"));
	CString sql;

	switch (m_nType) {

	case TYPE_OP_EDIT:
		//编辑记录
		sql.Format(
		_T("UPDATE medical_record SET ")
		_T("patient_name='%s',height='%s',weight='%s',gender='%s',bloodtype='%s',patient_type='%s',")
		_T("doctor='%s',clinic='%s',address='%s',phone_number='%s',birthday='%s',admission_date='%s',operator='%s' ")
		_T("WHERE medical_num = '%s'"),
		sName, sHeight, sWeight, sGender, sBloodType, sPatientType,
		sDoctor, sClinic, sAddress, sPhoneNumber, sBirthday, sAdmissionDate, sOperator,
		m_sMedNum);
		break;
	
	case TYPE_OP_NEW:
		//插入新记录
		sql.Format(
		_T("INSERT medical_record (medical_num,patient_name,height,weight,gender,bloodtype,patient_type,")
		_T("doctor,clinic,address,phone_number,birthday,admission_date,create_date,operator) ")
		_T("VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');"),
		m_sMaxMedNum, sName, sHeight, sWeight, sGender, sBloodType, sPatientType,
		sDoctor, sClinic, sAddress, sPhoneNumber, sBirthday, sAdmissionDate, sCreateDate, sOperator);
		break;
	}

	OutputDebugString(sql + _T("\n"));

	// 执行SQL语句
	CSmartLock lock(m_dbLock);
	try {
		m_db.ExecuteSQL(sql);
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CMedicalRecordEditDlg::OnBnClickedSave第1个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		MessageBox(_T("数据库操作失败！"), _T("提示"), MB_ICONERROR | MB_OK);
		return;
	}
	
	if (m_sMedNum.GetLength() > 0) {
		//编辑记录，仅仅刷新列表显示
		m_pListDlg->UpdateRecordList();
	}
	else {
		//插入新记录后，要增加列表计数
		m_pListDlg->IncreaseRecordCounter();
		m_pListDlg->OnBnClickedBtnLast();
		// 病历号加一
		IncreaseMedNum(m_sMaxMedNum);
		m_edtRecordNum.SetWindowText(m_sMaxMedNum);
	}
}

void CMedicalRecordEditDlg::GetMaxMedNum(CString& s)
{
	CRecordset rs(&m_db);
	CString sql = _T("SELECT TOP 1 medical_num FROM medical_record ORDER BY medical_num DESC");

	OutputDebugString(sql + _T("\n"));

	// 执行SQL语句
	CSmartLock lock(m_dbLock);
	try {
		rs.Open(AFX_DB_USE_DEFAULT_TYPE, sql);
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CMedicalRecordEditDlg::GetMaxMedNum第1个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		m_sMaxMedNum = DEFAULT_MEDICAL_NUM_START;
		return;
	}

	// 读出内容为最大的medno
	if (!rs.IsEOF()) {
		rs.MoveFirst();
		try {
			rs.GetFieldValue(_T("medical_num"), m_sMaxMedNum);
		}
		catch (CDBException* pe) {
			_TCHAR err[255];
			pe->GetErrorMessage(err, 255);
			theApp.WriteLog(_T("异常位于：CMedicalRecordEditDlg::GetMaxMedNum第2个catch\n"));
			theApp.WriteLog(err);
			theApp.WriteLog(_T("\n"));
			pe->Delete();
		}
	}
	else {
		m_sMaxMedNum = DEFAULT_MEDICAL_NUM_START;
	}

	if (rs.IsOpen()) {
		rs.Close();
	}
}

void CMedicalRecordEditDlg::IncreaseMedNum(CString& s)
{
	IncreaseStringNum(s);
}

void CMedicalRecordEditDlg::OnBnClickedCancel()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialogEx::OnCancel();
}

BOOL CMedicalRecordEditDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	int i;

	//设置输入框限长
	m_edtName.SetLimitText(24);
	m_edtAddress.SetLimitText(64);
	m_edtClinic.SetLimitText(24);
	m_edtDoctor.SetLimitText(24);
	m_edtHeight.SetLimitText(4);
	m_edtWeight.SetLimitText(4);
	m_edtPhoneNumber.SetLimitText(24);

	//设置combo控件的选项
	m_cmbGender.ResetContent();
	for (i=0; i<CFMDict::GetGenderCount(); i++) {
		m_cmbGender.AddString(CFMDict::Gender_itos(i));
	}
	m_cmbType.ResetContent();
	for (i=0; i<CFMDict::GetPatientTypeCount(); i++) {
		m_cmbType.AddString(CFMDict::PatientType_itos(i));
	}
	m_cmbBloodType.ResetContent();
	for (i=0; i<CFMDict::GetBloodTypeCount(); i++) {
		m_cmbBloodType.AddString(CFMDict::BloodType_itos(i));
	}
	CTime tmNow = CTime::GetCurrentTime();
	m_datAdmissionDate.SetTime(&tmNow);

	//病历号是只读的。无论新建还是编辑，都不允许手工调整病历号
	m_edtRecordNum.SetReadOnly(TRUE);

	//设置时间显示格式
	m_dateBirthday.SetFormat(_T("yyyy-MM-dd"));
	m_datAdmissionDate.SetFormat(_T("yyyy-MM-dd"));

	switch (m_nType) {

	case TYPE_OP_EDIT:
		// 编辑状态，查询数据库并初始化控件内容
		this->SetWindowText(_T("编辑记录"));
		m_edtRecordNum.SetWindowText(m_sMedNum);
		// 执行查询，并根据查询结果填写控件内容
		QueryRecord();
		break;

	case TYPE_OP_NEW:
		// 新建状态，取得最大的病历号，并把它加一
		GetMaxMedNum(m_sMaxMedNum);
		IncreaseMedNum(m_sMaxMedNum);
		m_edtRecordNum.SetWindowText(m_sMaxMedNum);
		this->SetWindowText(_T("新建记录"));
		break;
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CMedicalRecordEditDlg::QueryRecord()
{
	CRecordset rs(&m_db);
	CString sql;
	sql.Format(
		_T("SELECT patient_name,patient_type,gender,bloodtype,height,weight,birthday,address,phone_number,clinic,doctor,admission_date ")
		_T("FROM medical_record WHERE medical_num = '%s'"), m_sMedNum);

	// 执行SQL语句
	CSmartLock lock(m_dbLock);
	try {
		rs.Open(AFX_DB_USE_DEFAULT_TYPE, sql);
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CMedicalRecordEditDlg::QueryRecord第1个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		return;
	}

	//将内容填入右侧控件
	try {
		rs.MoveFirst();
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CMedicalRecordEditDlg::QueryRecord第2个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		return;
	}
	if (!rs.IsEOF()) {
		try {
			CTime tm;
			CString sValue;
			CDBVariant var;

			rs.GetFieldValue(_T("patient_name"), sValue);
			m_edtName.SetWindowText(sValue);

			rs.GetFieldValue(_T("patient_type"), var);
			m_cmbType.SetCurSel(var.m_lVal);

			rs.GetFieldValue(_T("gender"), var);
			m_cmbGender.SetCurSel(var.m_lVal);

			rs.GetFieldValue(_T("bloodtype"), var);
			m_cmbBloodType.SetCurSel(var.m_lVal);

			rs.GetFieldValue(_T("height"), var);
			sValue.Format(_T("%d"), var.m_lVal);
			m_edtHeight.SetWindowText(sValue);

			rs.GetFieldValue(_T("weight"), var);
			sValue.Format(_T("%d"), var.m_lVal);
			m_edtWeight.SetWindowText(sValue);

			rs.GetFieldValue(_T("birthday"), var);
			tm = CTime((var.m_pdate)->year, (var.m_pdate)->month, (var.m_pdate)->day,
				(var.m_pdate)->hour, (var.m_pdate)->minute, (var.m_pdate)->second);
			m_dateBirthday.SetTime(&tm);

			rs.GetFieldValue(_T("address"), sValue);
			m_edtAddress.SetWindowText(sValue);

			rs.GetFieldValue(_T("phone_number"), sValue);
			m_edtPhoneNumber.SetWindowText(sValue);

			rs.GetFieldValue(_T("clinic"), sValue);
			m_edtClinic.SetWindowText(sValue);

			rs.GetFieldValue(_T("doctor"), sValue);
			m_edtDoctor.SetWindowText(sValue);

			rs.GetFieldValue(_T("admission_date"), var);
			tm = CTime((var.m_pdate)->year, (var.m_pdate)->month, (var.m_pdate)->day,
				(var.m_pdate)->hour, (var.m_pdate)->minute, (var.m_pdate)->second);
			m_datAdmissionDate.SetTime(&tm);
		}
		catch (CDBException* pe) {
			_TCHAR err[255];
			pe->GetErrorMessage(err, 255);
			theApp.WriteLog(_T("异常位于：CMedicalRecordEditDlg::QueryRecord第3个catch\n"));
			theApp.WriteLog(err);
			theApp.WriteLog(_T("\n"));
			pe->Delete();
			return;
		}
	}

	if (rs.IsOpen()) {
		rs.Close();
	}
}

