// AdvancedSetupDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "FMCenter.h"
#include "AdvancedSetupDlg.h"
#include "afxdialogex.h"


// CAdvancedSetupDlg 对话框
#define ADMIN_TYPE_NORMAL              (_T(""))
#define ADMIN_TYPE_ADVANCE             (_T("高级"))
#define ADMIN_LIST_COLUMN_COUNT        (3)
#define ADMIN_INDEX_OF_NAME            (1)
#define ADMIN_INDEX_OF_TYPE            (2)

//column上面显示的文字
static _TCHAR* l_arColumnLabel[ADMIN_LIST_COLUMN_COUNT] =
{
	_T("序号"),
	_T("姓名"),
	_T("级别")
};
//column上文字的显示方式（靠左）
static int l_arColumnFmt[ADMIN_LIST_COLUMN_COUNT] = 
{
	LVCFMT_RIGHT,
	LVCFMT_LEFT,
	LVCFMT_LEFT
};
//column的宽度
static int l_arColumnWidth[ADMIN_LIST_COLUMN_COUNT] = 
{
	60,
	120,
	60
};


IMPLEMENT_DYNAMIC(CAdvancedSetupDlg, CDialogEx)

CAdvancedSetupDlg::CAdvancedSetupDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CAdvancedSetupDlg::IDD, pParent)
	, m_db(theApp.m_db)
	, m_dbLock(theApp.m_csDBLock)
{
	m_nListCurrentIndex = -1;
}

CAdvancedSetupDlg::~CAdvancedSetupDlg()
{
}

void CAdvancedSetupDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_HOSPITAL_NAME, m_edtHospitalName);
	DDX_Control(pDX, IDC_LST_ADMIN_LIST, m_lstAdminList);
	DDX_Control(pDX, IDC_EDT_ADMIN_NAME, m_edtAdminName);
	DDX_Control(pDX, IDC_CHK_ADMIN_TYPE, m_chkAdminType);
	DDX_Control(pDX, ID_BTN_ADD_ADMIN, m_btnAddAdmin);
	DDX_Control(pDX, ID_BTN_EDIT_ADMIN, m_btnEditAdmin);
	DDX_Control(pDX, ID_BTN_DEL_ADMIN, m_btnDelAdmin);
	DDX_Control(pDX, IDC_EDT_ADMIN_PASS1, m_edtPass1);
	DDX_Control(pDX, IDC_EDT_ADMIN_PASS2, m_edtPass2);
	DDX_Control(pDX, IDC_COMBO_FHR_PAPER_SPEED, m_cmbFHRPaperSpeed);
	DDX_Control(pDX, IDC_COMBO_NIBP_UNIT, m_cmbNIBPUnit);
	DDX_Control(pDX, IDC_COMBO_TEMP_UNIT, m_cmbTEMPUnit);
	DDX_Control(pDX, IDC_CHK_TEND_PRINT_HR, m_chkTendPrintHR);
	DDX_Control(pDX, IDC_CHK_TEND_PRINT_SYS, m_chkTendPrintSYS);
	DDX_Control(pDX, IDC_CHK_TEND_PRINT_MEA, m_chkTendPrintMEA);
	DDX_Control(pDX, IDC_CHK_TEND_PRINT_DIA, m_chkTendPrintDIA);
	DDX_Control(pDX, IDC_CHK_TEND_PRINT_RR, m_chkTendPrintRR);
	DDX_Control(pDX, IDC_CHK_TEND_PRINT_SPO2, m_chkTendPrintSPO2);
	DDX_Control(pDX, IDC_CHK_TEND_PRINT_T1, m_chkTendPrintT1);
	DDX_Control(pDX, IDC_CHK_TEND_PRINT_T2, m_chkTendPrintT2);
	DDX_Control(pDX, IDC_COMBO_FHR_REPORT_TYPE, m_cmbFHRReportType);
}

BEGIN_MESSAGE_MAP(CAdvancedSetupDlg, CDialogEx)
	ON_NOTIFY(NM_CLICK, IDC_LST_ADMIN_LIST, &CAdvancedSetupDlg::OnItemclickLstAdmin)
	ON_BN_CLICKED(ID_BTN_ADD_ADMIN, &CAdvancedSetupDlg::OnBnClickedBtnAddAdmin)
	ON_BN_CLICKED(ID_BTN_EDIT_ADMIN, &CAdvancedSetupDlg::OnBnClickedBtnEditAdmin)
	ON_BN_CLICKED(ID_BTN_DEL_ADMIN, &CAdvancedSetupDlg::OnBnClickedBtnDelAdmin)
	ON_BN_CLICKED(IDOK, &CAdvancedSetupDlg::OnBnClickedOk)
	ON_BN_CLICKED(ID_BTN_SAVE_SYSTEM_SETUP, &CAdvancedSetupDlg::OnBnClickedBtnSaveSystemSetup)
END_MESSAGE_MAP()


// CAdvancedSetupDlg 消息处理程序
void CAdvancedSetupDlg::OnBnClickedBtnAddAdmin()
{
	CString sPass1;
	CString sPass2;
	CString sName;
	int nType;

	m_edtAdminName.GetWindowText(sName);
	m_edtPass1.GetWindowText(sPass1);
	m_edtPass2.GetWindowText(sPass2);
	nType = m_chkAdminType.GetCheck();

	if (CheckAdminInfo(sName, sPass1, sPass2)) {
		AddNewAdmin(sName, sPass1, nType);
		UpdateAdminList();
	}
}

void CAdvancedSetupDlg::OnBnClickedBtnEditAdmin()
{
	CString sPass1;
	CString sPass2;
	CString sName;
	int nType;
	int nID;

	if (-1 == m_nListCurrentIndex) {
		return;
	}

	m_edtAdminName.GetWindowText(sName);
	m_edtPass1.GetWindowText(sPass1);
	m_edtPass2.GetWindowText(sPass2);
	nType = m_chkAdminType.GetCheck();
	nID = (int)m_lstAdminList.GetItemData(m_nListCurrentIndex);

	if (CheckAdminInfo(sName, sPass1, sPass2)) {
		UpdateAdmin(nID, sName, sPass1, nType);
		UpdateAdminList();
	}
}

void CAdvancedSetupDlg::OnBnClickedBtnDelAdmin()
{
	int nID;
	if (-1 == m_nListCurrentIndex) {
		return;
	}

	nID = (int)m_lstAdminList.GetItemData(m_nListCurrentIndex);
	DelAdmin(nID);
	UpdateAdminList();
}

void CAdvancedSetupDlg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialogEx::OnOK();
}

BOOL CAdvancedSetupDlg::CheckAdminInfo(CString& sName, CString& sPass1, CString& sPass2)
{
	if (sName.IsEmpty()) {
		MessageBox(_T("请输入管理员姓名！"));
		m_edtAdminName.SetFocus();
		return FALSE;
	}
	if (-1 != sName.FindOneOf(_T("\',\",%,\\;,="))) {
		MessageBox(_T("用户名不能使用\',\",%,\\;,=等符号！"));
		m_edtAdminName.SetFocus();
		return FALSE;
	}
	if (sPass1 != sPass2) {
		MessageBox(_T("两个密码不一致！"));
		return FALSE;
	}
	if (sPass1.IsEmpty()) {
		MessageBox(_T("请输入密码！"));
		m_edtPass1.SetFocus();
		return FALSE;
	}
	if (-1 != sPass1.FindOneOf(_T("\',\",%,\\;,="))) {
		MessageBox(_T("密码中不能使用\',\",%,\\;,=等符号！"));
		m_edtPass1.SetFocus();
		return FALSE;
	}
	
	int i;
	int count = m_lstAdminList.GetItemCount();
	for (i=0; i<count; i++) {
		if (i == m_nListCurrentIndex) {
			continue; //当前正在编辑的这个不管重名
		}
		if (0 == sName.Compare(m_lstAdminList.GetItemText(i, ADMIN_INDEX_OF_NAME))) {
			MessageBox(_T("管理员重名！"));
			m_edtAdminName.SetFocus();
			return FALSE;
		}
	}

	return TRUE;
}

BOOL CAdvancedSetupDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	int i;

	// 设置编辑框的输入长度
	m_edtHospitalName.SetLimitText(48);
	m_edtAdminName.SetLimitText(24);
	m_edtPass1.SetLimitText(24);
	m_edtPass2.SetLimitText(24);

	m_chkTendPrintHR.SetCheck(theApp.m_bTendPrintHR);
	m_chkTendPrintSYS.SetCheck(theApp.m_bTendPrintSYS);
	m_chkTendPrintMEA.SetCheck(theApp.m_bTendPrintMEA);
	m_chkTendPrintDIA.SetCheck(theApp.m_bTendPrintDIA);
	m_chkTendPrintRR.SetCheck(theApp.m_bTendPrintRR);
	m_chkTendPrintSPO2.SetCheck(theApp.m_bTendPrintSPO2);
	m_chkTendPrintT1.SetCheck(theApp.m_bTendPrintT1);
	m_chkTendPrintT2.SetCheck(theApp.m_bTendPrintT2);

	m_cmbFHRPaperSpeed.ResetContent();
	m_cmbNIBPUnit.ResetContent();
	m_cmbTEMPUnit.ResetContent();
	m_cmbFHRReportType.ResetContent();
	int count = CFMDict::GetFHRPaperSpeedOptCount();
	for (i=0; i<count; i++) {
		m_cmbFHRPaperSpeed.AddString(CFMDict::FHRPaperSpeedOpt_itos(i));
	}
	count = CFMDict::GetNIBPUnitCount();
	for (i=0; i<count; i++) {
		m_cmbNIBPUnit.AddString(CFMDict::NIBPUnit_itos(i));
	}
	count = CFMDict::GetTEMPUnitCount();
	for (i=0; i<count; i++) {
		m_cmbTEMPUnit.AddString(CFMDict::TEMPUnit_itos(i));
	}
	count = CFMDict::GetFHRReportTypeCount();
	for (i=0; i<count; i++) {
		m_cmbFHRReportType.AddString(CFMDict::FHRReportType_itos(i));
	}
	m_cmbFHRPaperSpeed.SetCurSel(theApp.m_nFHRSpeedOption);
	m_cmbNIBPUnit.SetCurSel(theApp.m_nNIBPUnitOption);
	m_cmbTEMPUnit.SetCurSel(theApp.m_nTEMPUnitOption);
	m_cmbFHRReportType.SetCurSel(theApp.m_nFHRReportType);
	m_edtHospitalName.SetWindowText(theApp.m_sHospitalName);

	// 初始化LIST的表头
	m_lstAdminList.RemoveAllGroups();
	m_lstAdminList.ModifyStyle(0, LVS_REPORT | LVS_SHOWSELALWAYS, 0);
	m_lstAdminList.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	//设定一个用于存取column的结构lvc
	LVCOLUMN lvc;
	//设定存取模式
	lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
	//用InSertColumn函数向窗口中插入柱
	for (i=0 ; i<ADMIN_LIST_COLUMN_COUNT; i++) {
		lvc.iSubItem = i;
		lvc.pszText = l_arColumnLabel[i];
		lvc.cx = l_arColumnWidth[i];
		lvc.fmt = l_arColumnFmt[i];
		m_lstAdminList.InsertColumn(i,&lvc);
	}

	UpdateAdminList();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CAdvancedSetupDlg::UpdateAdminList()
{
	// 执行查询
	CRecordset rs(&m_db);
	CString sWhereStatement;
	CString sql;

	m_lstAdminList.DeleteAllItems();
	m_nListCurrentIndex = -1;

	sql = _T("SELECT oname,otype,oid FROM operator_info");

	OutputDebugString(sql + _T("\n"));

	// 执行SQL语句
	CSmartLock lock(m_dbLock);
	try {
		rs.Open(AFX_DB_USE_DEFAULT_TYPE, sql);
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CAdvancedSetupDlg::UpdateAdminList第1个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		return;
	}

	// 将查询内容写入列表
	int nCount;
	try {
		while (! rs.IsEOF()) {
			rs.MoveNext();
		}
		nCount = rs.GetRecordCount();
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CAdvancedSetupDlg::UpdateAdminList第2个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		return;
	}
	
	if (0 == nCount) {
		rs.Close();
		return;
	}

	try {
		rs.MoveFirst();
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CAdvancedSetupDlg::UpdateAdminList第3个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		return;
	}

	int i = 0;
	while (!rs.IsEOF()) {
		
		LVITEM lvi;
		_TCHAR buffer[256];
		lvi.mask = LVIF_TEXT;
		lvi.iItem = i;
		lvi.pszText = buffer;
		lvi.cchTextMax = 255;
		i ++;

		try {
			CString sValue;
			CDBVariant var;

			sValue.Format(_T("%d"), i);
			lvi.iSubItem = 0;
			_tcsncpy(lvi.pszText, sValue.GetBuffer(0), 255);
			m_lstAdminList.InsertItem(&lvi);

			rs.GetFieldValue(_T("oname"), sValue);
			lvi.iSubItem = 1;
			_tcsncpy(lvi.pszText, sValue.GetBuffer(0), 255);
			m_lstAdminList.SetItem(&lvi);

			rs.GetFieldValue(_T("otype"), var);
			sValue = CFMDict::AdminType_itos(var.m_lVal);
			lvi.iSubItem = 2;
			_tcsncpy(lvi.pszText, sValue.GetBuffer(0), 255);
			m_lstAdminList.SetItem(&lvi);

			rs.GetFieldValue(_T("oid"), var);
			m_lstAdminList.SetItemData(lvi.iItem, (DWORD_PTR)var.m_lVal);
		}
		catch (CDBException* pe) {
			_TCHAR err[255];
			pe->GetErrorMessage(err, 255);
			theApp.WriteLog(_T("异常位于：CAdvancedSetupDlg::UpdateAdminList第4个catch\n"));
			theApp.WriteLog(err);
			theApp.WriteLog(_T("\n"));
			pe->Delete();
			return;
		}
		rs.MoveNext();
	}

	if (rs.IsOpen()) {
		rs.Close();
	}

	m_nListCurrentIndex = -1;
	SetButtonState();
}

void CAdvancedSetupDlg::SetButtonState()
{
	if (-1 == m_nListCurrentIndex) {
		m_btnEditAdmin.EnableWindow(FALSE);
		m_btnDelAdmin.EnableWindow(FALSE);
	}
	else {
		m_btnEditAdmin.EnableWindow(TRUE);
		m_btnDelAdmin.EnableWindow(TRUE);
	}
}

void CAdvancedSetupDlg::AddNewAdmin(CString& sName, CString& sPass, int nType)
{
	CString sql;
	sql.Format(
		_T("INSERT operator_info (otype,oname,opass) ")
		_T("VALUES ('%d','%s','%s');"),
		nType, sName, sPass);

	OutputDebugString(sql + _T("\n"));

	// 执行SQL语句
	CSmartLock lock(m_dbLock);
	try {
		m_db.ExecuteSQL(sql);
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CAdvancedSetupDlg::AddNewAdmin第1个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		MessageBox(_T("数据库操作失败！"), _T("提示"), MB_ICONERROR | MB_OK);
		return;
	}
}

void CAdvancedSetupDlg::UpdateAdmin(int nID, CString& sName, CString& sPass, int nType)
{
	CString sql;
	sql.Format(
		_T("UPDATE operator_info ")
		_T("SET otype='%d',oname='%s',opass='%s' ")
		_T("WHERE oid = '%d'"),
		nType, sName, sPass, nID);

	OutputDebugString(sql + _T("\n"));

	// 执行SQL语句
	CSmartLock lock(m_dbLock);
	try {
		m_db.ExecuteSQL(sql);
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CAdvancedSetupDlg::UpdateAdmin第1个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		MessageBox(_T("数据库操作失败！"), _T("提示"), MB_ICONERROR | MB_OK);
		return;
	}
}

void CAdvancedSetupDlg::DelAdmin(int nID)
{
	CString sql;
	sql.Format(
		_T("DELETE FROM operator_info WHERE oid = '%d'"),
		nID);

	OutputDebugString(sql + _T("\n"));

	// 执行SQL语句
	CSmartLock lock(m_dbLock);
	try {
		m_db.ExecuteSQL(sql);
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CAdvancedSetupDlg::DelAdmin第1个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		MessageBox(_T("数据库操作失败！"), _T("提示"), MB_ICONERROR | MB_OK);
		return;
	}
}

void CAdvancedSetupDlg::OnItemclickLstAdmin(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMHEADER phdr = reinterpret_cast<LPNMHEADER>(pNMHDR);
	*pResult = 0;

	// 得到选中的行数
	int nIndex = phdr->iItem;
	if (nIndex < 0 || nIndex >= m_lstAdminList.GetItemCount()) {
		m_nListCurrentIndex = -1;
		SetButtonState();
        return;
    }
	m_nListCurrentIndex = nIndex;
	SetButtonState();

	CString sName = m_lstAdminList.GetItemText(nIndex, ADMIN_INDEX_OF_NAME);
	int nType = CFMDict::AdminType_stoi(m_lstAdminList.GetItemText(nIndex, ADMIN_INDEX_OF_TYPE));

	m_edtAdminName.SetWindowText(sName);
	m_chkAdminType.SetCheck(nType);
	m_edtPass1.Clear();
	m_edtPass2.Clear();
}

void CAdvancedSetupDlg::OnBnClickedBtnSaveSystemSetup()
{
	CString sHospitalName;
	int nFHRSpeedOption;
	int nNIBPUnitOption;
	int nTEMPUnitOption;
	int nFHRReportType;

	m_edtHospitalName.GetWindowText(sHospitalName);
	nFHRSpeedOption = m_cmbFHRPaperSpeed.GetCurSel();
	nNIBPUnitOption = m_cmbNIBPUnit.GetCurSel();
	nTEMPUnitOption = m_cmbTEMPUnit.GetCurSel();
	nFHRReportType  = m_cmbFHRReportType.GetCurSel();

	if (sHospitalName.IsEmpty()) {
		int nRet = MessageBox(_T("不输入医院名称，会影响到打印报告中的医院信息。\n你确定保持医院名称为空吗？"),
			_T("提示信息"), MB_ICONEXCLAMATION | MB_YESNO);
		if (IDYES != nRet) {
			m_edtHospitalName.GetFocus();
			return;
		}
	}

	if (CB_ERR == nFHRSpeedOption) {
		MessageBox(_T("请选择有效的走纸速度！"),
			_T("提示信息"), MB_ICONEXCLAMATION | MB_OK);
		m_cmbFHRPaperSpeed.GetFocus();
		return;
	}

	if (CB_ERR == nNIBPUnitOption) {
		MessageBox(_T("请选择有效的血压单位！"),
			_T("提示信息"), MB_ICONEXCLAMATION | MB_OK);
		m_cmbNIBPUnit.GetFocus();
		return;
	}

	if (CB_ERR == nTEMPUnitOption) {
		MessageBox(_T("请选择有效的温度单位！"),
			_T("提示信息"), MB_ICONEXCLAMATION | MB_OK);
		m_cmbTEMPUnit.GetFocus();
		return;
	}

	theApp.m_bTendPrintHR   = m_chkTendPrintHR.GetCheck();
	theApp.m_bTendPrintSYS  = m_chkTendPrintSYS.GetCheck();
	theApp.m_bTendPrintMEA  = m_chkTendPrintMEA.GetCheck();
	theApp.m_bTendPrintDIA  = m_chkTendPrintDIA.GetCheck();
	theApp.m_bTendPrintRR   = m_chkTendPrintRR.GetCheck();
	theApp.m_bTendPrintSPO2 = m_chkTendPrintSPO2.GetCheck();
	theApp.m_bTendPrintT1   = m_chkTendPrintT1.GetCheck();
	theApp.m_bTendPrintT2   = m_chkTendPrintT2.GetCheck();
	theApp.m_sHospitalName  = sHospitalName;
	theApp.m_nFHRSpeedOption = nFHRSpeedOption;
	theApp.m_nNIBPUnitOption = nNIBPUnitOption;
	theApp.m_nTEMPUnitOption = nTEMPUnitOption;
	theApp.m_nFHRReportType  = nFHRReportType;
	if (theApp.SaveSystemSetup()) {
		MessageBox(_T("保存成功！"), _T("提示信息"), MB_OK);
	}
	else {
		MessageBox(_T("保存失败，请检查您系统的 数据库 设置！"),
			_T("提示信息"), MB_ICONERROR | MB_OK);
	}
}
