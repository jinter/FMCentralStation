
// FMCenter.cpp : 定义应用程序的类行为。
//

#include "stdafx.h"
#include "afxwinappex.h"
#include "afxdialogex.h"
#include "FMCenter.h"
#include "MainFrm.h"
#include "SplashWnd.h"
#include "LoginDlg.h"
#include "LogFile.h"
#include "FMMonitorView.h"
#include "FMToolbarForm.h"
#include "FMSocketThread.h"
#include "FMRecordThread.h"
#include "FMRecordUnit.h"
#include "FHRAutodiagnostics.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CFMCenterApp

BEGIN_MESSAGE_MAP(CFMCenterApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, &CFMCenterApp::OnAppAbout)
	// 基于文件的标准文档命令
	ON_COMMAND(ID_FILE_NEW, &CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, &CWinApp::OnFileOpen)
	// 标准打印设置命令
	ON_COMMAND(ID_FILE_PRINT_SETUP, &CWinApp::OnFilePrintSetup)
END_MESSAGE_MAP()


// CFMCenterApp 构造
CFMCenterApp::CFMCenterApp()
	: m_strDBName(FMDB_DBNAME)
{
	// TODO: 将以下应用程序 ID 字符串替换为唯一的 ID 字符串；建议的字符串格式
	//为 CompanyName.ProductName.SubProduct.VersionInformation
	SetAppID(_T("FMCenter.AppID.NoVersion"));

	// TODO: 在此处添加构造代码，
	// 将所有重要的初始化放置在 InitInstance 中
	m_pSocketThread = NULL;
	m_pRecordThread = NULL;
	m_bOK = FALSE;
	m_pLogFile = NULL;

	m_bAutoDiagno = FALSE;
	m_bDebugUI = FALSE;
	m_bDebugArea = FALSE;
	m_bDebugSocket = FALSE;
	m_bDebugStorage = FALSE;
	m_bDebugNoLogin = FALSE;

	m_pPrintFrame = NULL;

	m_nFHRSpeedOption = 0;
	m_nNIBPUnitOption = 0;
	m_nTEMPUnitOption = 0;
	m_nFHRReportType  = 0;
	m_bTendPrintHR   = TRUE;
	m_bTendPrintSYS  = TRUE;
	m_bTendPrintMEA  = TRUE;
	m_bTendPrintDIA  = TRUE;
	m_bTendPrintRR   = TRUE;
	m_bTendPrintSPO2 = TRUE;
	m_bTendPrintT1   = TRUE;
	m_bTendPrintT2   = TRUE;
}

// 唯一的一个 CFMCenterApp 对象

CFMCenterApp theApp;

int CFMCenterApp::GetNoOfProcessors()
{
	static int nProcessors = 0;
	if (0 == nProcessors) {
		SYSTEM_INFO si;
		GetSystemInfo(&si);
		nProcessors = si.dwNumberOfProcessors;
	}
	return nProcessors;
}

// CFMCenterApp 初始化
BOOL CFMCenterApp::InitInstance()
{
	m_hMutex = ::CreateMutex(NULL, FALSE, m_pszAppName);
	if (::GetLastError() == ERROR_ALREADY_EXISTS) {
		::CloseHandle(m_hMutex);
		HWND hPre = ::GetWindow(::GetDesktopWindow(), GW_CHILD);
		while (::IsWindow(hPre)) {
			if (::GetPropW(hPre, m_pszAppName)) {
				if (::IsIconic(hPre)) {
					::ShowWindow(hPre, SW_RESTORE);
				}
				::SetForegroundWindow(hPre);
				::SetForegroundWindow(::GetLastActivePopup(hPre));
				return FALSE;
			}
			hPre = ::GetWindow(hPre, GW_HWNDNEXT);
		}		
	}

	// 如果一个运行在 Windows XP 上的应用程序清单指定要
	// 使用 ComCtl32.dll 版本 6 或更高版本来启用可视化方式，
	//则需要 InitCommonControlsEx()。否则，将无法创建窗口。
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 将它设置为包括所有要在应用程序中使用的
	// 公共控件类。
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}
	
	//打开配置文件
	if (! OpenConfig()) {
		MessageBox(NULL, _T("读取 配置文件 失败！\n\n程序将关闭！"), _T("错误信息"), MB_OK);
		return FALSE;
	}
	//创建日志文件
	CString sLogFile;
	sLogFile.Format(_T("%s%s"), theApp.GetDataRoot(), LOG_FILE);
	//AfxMessageBox(sLogFile);
	m_pLogFile = new CLogFile(sLogFile);
	
	//预留自动诊断模块接口
	CString sDllPath;
	//sDllPath.Format(_T("%s%s"), theApp.GetDataRoot(), _T("动态库路径"));
	//if (! CFHRAutodiagnostics::InitModule(sDllPath)) {
	//	MessageBox(NULL, _T("自动诊断模块 加载失败！\n\n程序将关闭自动诊断功能！"), _T("错误信息"), MB_OK);
		m_bAutoDiagno = FALSE;
	//} else {
	//	m_bAutoDiagno = TRUE;
	//}

	if (! DebugUI()) {//测试模式，不创建数据库，防止因误操作修改了数据库
		if (! CreateDB()) {
			MessageBox(NULL, _T("创建 数据库 失败！\n\n程序将关闭！"), _T("错误信息"), MB_OK);
			return FALSE;
		}
	}

	if (DebugNoLogin()) {
		//测试代码
		theApp.m_nAdminID = 3;
		theApp.m_sAdminName = _T("test");
		theApp.m_nAdminType = 1;
		//测试结束
	}
	else {
		//登录对话框
		CLoginDlg dlg;
		if (IDOK != dlg.DoModal()) {
			return FALSE;
		}
	}
	//读出系统设置
	if (! DebugUI()) {
		if (! InitSystemSetup()) {
			m_sHospitalName = _T("");
		}
	}

	m_pRecordThread = new CFMRecordThread();
	if (!m_pRecordThread->Start()) {
		MessageBox(NULL, _T("创建 记录 线程失败！"), _T("错误信息"), MB_OK);
		return FALSE;
	}

	EnableTaskbarInteraction(FALSE);

	// 使用 RichEdit 控件需要  AfxInitRichEdit2()	
	// AfxInitRichEdit2();

	// 标准初始化
	// 如果未使用这些功能并希望减小
	// 最终可执行文件的大小，则应移除下列
	// 不需要的特定初始化例程
	// 更改用于存储设置的注册表项
	// TODO: 应适当修改该字符串，
	// 例如修改为公司或组织名
	SetRegistryKey(_T("应用程序向导生成的本地应用程序"));
	LoadStdProfileSettings(0);  // 加载标准 INI 文件选项(包括 MRU)

	// 若要创建主窗口，此代码将创建新的框架窗口
	// 对象，然后将其设置为应用程序的主窗口对象
	CMainFrame* pFrame = new CMainFrame();
	if (!pFrame) {
		return FALSE;
	}
	m_pMainWnd = pFrame;
	// 创建主 MDI 框架窗口
	if (!pFrame->LoadFrame(IDR_MAINFRAME)) {
		return FALSE;
	}

	// 分析标准 shell 命令、DDE、打开文件操作的命令行
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	// 显示LOGO窗口
	CSplashWnd::EnableSplashScreen(cmdInfo.m_bShowSplash);
	CSplashWnd::ShowSplashScreen(m_pMainWnd);

	// 调度在命令行中指定的命令。如果
	// 用 /RegServer、/Register、/Unregserver 或 /Unregister 启动应用程序，则返回 FALSE。
	if (! ProcessShellCommand(cmdInfo)) {
		return FALSE;
	}

	// 唯一的一个窗口已初始化，因此显示它并对其进行更新
	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();
	// 仅当具有后缀时才调用 DragAcceptFiles
	//  在 SDI 应用程序中，这应在 ProcessShellCommand 之后发生
	// 启用拖/放
	//m_pMainWnd->DragAcceptFiles();

	m_bOK = TRUE; //到这里，就说明资源准备好了，可以互相调用了
	// 都准备好之后，才能打开监听端口
	m_pSocketThread = new CFMSocketThread(m_pRecordThread);
	if (!m_pSocketThread->Start()) {
		MessageBox(NULL, _T("创建 监听 端口失败！"), _T("错误信息"), MB_OK);
		return FALSE;
	}

	return TRUE;
}

int CFMCenterApp::ExitInstance()
{
	if (m_pSocketThread) {
		delete m_pSocketThread;
		m_pSocketThread = NULL;
	}
	if (m_pRecordThread) {
		delete m_pRecordThread;
		m_pRecordThread = NULL;
	}
	if (m_db.IsOpen()) {
		m_db.Close();
	}
	::CloseHandle(m_hMutex);
	if (m_pLogFile) {
		delete m_pLogFile;
		m_pLogFile = NULL;
	}
	if (! m_arAliveBedIndex.IsEmpty()) {
		m_arAliveBedIndex.RemoveAll();
	}
	return CWinApp::ExitInstance();
}

void CFMCenterApp::WriteLog(CString s)
{
	CSmartLock lock(m_csLogLock);
	if (m_pLogFile) {
		m_pLogFile->Write(s);
	}
}

int CFMCenterApp::GetFocusIndex()
{
	if (m_pToolbarForm) {
		return m_pToolbarForm->GetFocusIndex();
	}
	else {
		WriteLog(_T("CFMCenterApp::GetFocusIndex函数中，m_pToolbarForm无效！\n"));
		return 0;
	}
}

void CFMCenterApp::SetFocusIndex(int nCurIndex)
{
	//该函数的功能是修改FMToolbarForm的combo控件的当前选项
	m_pToolbarForm->SetComboSel(nCurIndex);
}

void CFMCenterApp::AddAliveIndex(int nBedIndex)
{
	CSmartLock lock(m_csAliveList);
	//排重处理
	int i;
	int count = m_arAliveBedIndex.GetCount();
	for (i=0; i<count; i++) {
		if (nBedIndex == m_arAliveBedIndex[i]) {
			m_arAliveBedIndex.RemoveAt(i);
			count --;
			i --;
		}
	}
	int insert_index = m_arAliveBedIndex.GetCount();
	m_arAliveBedIndex.InsertAt(insert_index, nBedIndex);
	PostSocketMessage();
}

void CFMCenterApp::DelAliveIndex(int nBedIndex)
{
	CSmartLock lock(m_csAliveList);
	int i;
	int count = m_arAliveBedIndex.GetCount();
	for (i=0; i<count; i++) {
		if (nBedIndex == m_arAliveBedIndex[i]) {
			m_arAliveBedIndex.RemoveAt(i);
			break;
		}
	}
	PostSocketMessage();
}

int CFMCenterApp::GetAliveIndexList(CUIntArray& ar)
{
	CSmartLock lock(m_csAliveList);
	if (! ar.IsEmpty()) {
		ar.RemoveAll();
	}
	ar.Append(m_arAliveBedIndex);
	return ar.GetCount();
}

CFMRecordUnit* CFMCenterApp::FindRecordUnitByMRID(CString sMRID)
{
	if (! m_bOK) {
		return NULL;
	}
	int i;
	for (i = 0; i < CLIENT_COUNT; i ++) {
		CFMRecordUnit* pu = m_pRecordThread->GetUnit(i);
		if (pu->IsAlive()) {
			if (pu->GetRecordID() == sMRID) {
				return pu;
			}
		}
	}
	return NULL;
}

BOOL CFMCenterApp::CreateDB()
{
	//初始化数据库连接
	if (m_db.IsOpen()) {
		// 数据库已经打开
		return TRUE;
	}

	CString strConnection = _T("DSN=") + theApp.m_strDBName + _T(";");

	try {
		m_db.OpenEx(strConnection, CDatabase::useCursorLib);
	} catch (CDBException* pe) {
		pe->Delete();
		if (m_db.IsOpen()) {
			m_db.Close();
		}
		return FALSE;
	}
	
	return TRUE;
}

BOOL CFMCenterApp::OpenConfig()
{
	CString strFullPath = theApp.m_pszHelpFilePath;
	CString sLocalPath = GetFilePath(strFullPath);
	CString sConfigFileName;
	sConfigFileName.Format(_T("%s%s"), sLocalPath, CONFIG_FILE);

	CFile f;
	if (! f.Open(sConfigFileName, CFile::modeReadWrite)) {
		CString err;
		err.Format(_T("无法打开配置文件%s！将生成默认配置文件。"), CONFIG_FILE);
		AfxMessageBox(err);

		m_sDataRootPath = _T("D:/FMDATA");
		m_nListenPort = 5000;
		m_bDebugUI = TRUE;
		m_bDebugArea = FALSE;
		m_bDebugSocket = FALSE;
		m_bDebugStorage = FALSE;
		m_bDebugNoLogin = TRUE;
		m_bDebugSocketDetail = FALSE;

		f.Open(sConfigFileName, CFile::modeCreate | CFile::modeWrite);
		BYTE head[2] = {0xff, 0xfe};
		f.Write(head, 2);
		WCHAR content[] = _T("ROOT = D:/FMDATA\r\n")
						  _T("PORT = 5000\r\n")
						  _T("DEBUG_UI = ON\r\n")
						  _T("DEBUG_AREA = OFF\r\n")
						  _T("DEBUG_SOCKET = OFF\r\n")
						  _T("DEBUG_STORAGE = OFF\r\n")
						  _T("DEBUG_NO_LOGIN = ON\r\n")
						  _T("DEBUG_SOCKET_DETAIL = OFF\r\n");
		f.Write(content, sizeof(content));
		f.Flush();
		return TRUE;
	}

	int len = (int)f.GetLength();
	BYTE* data = new BYTE[len + 2];
	f.Read(data, len);
	f.Close();
	if (data[0] != 0xff || data[1] != 0xfe) {
		delete[] data;
		CString err;
		err.Format(_T("配置文件%s不是UNICODE格式！\n\n请另存为UNICODE格式后重试。"), CONFIG_FILE);
		AfxMessageBox(err);
		return FALSE;
	}

	data[len] = 0x0;
	data[len + 1] = 0x0;
	CString sIniContent = (_TCHAR*)(data + 2);//跳过文件开头的FF FE
	CIniString config(sIniContent);

	m_sDataRootPath = config.Get(_T("ROOT"));
	CString sPort = config.Get(_T("PORT"));
	CString sDebugUI = config.Get(_T("DEBUG_UI"));
	CString sDebugArea = config.Get(_T("DEBUG_AREA"));
	CString sDebugSocket = config.Get(_T("DEBUG_SOCKET"));
	CString sDebugStorage = config.Get(_T("DEBUG_STORAGE"));
	CString sDebugNoLogin = config.Get(_T("DEBUG_NO_LOGIN"));
	CString sDebugSocketDetail = config.Get(_T("DEBUG_SOCKET_DETAIL"));
	delete[] data;

	if (m_sDataRootPath.IsEmpty()) {
		CString err;
		err.Format(_T("配置文件%s中没有找到ROOT内容！"), CONFIG_FILE);
		AfxMessageBox(err);
		return FALSE;
	}
	if (sPort.IsEmpty()) {
		CString err;
		err.Format(_T("配置文件%s中没有找到PORT内容！"), CONFIG_FILE);
		AfxMessageBox(err);
		return FALSE;
	}
	m_nListenPort = _ttoi(sPort);
	if (m_nListenPort < 1025 || m_nListenPort > 65535) {
		CString err;
		err.Format(_T("配置文件%s中PORT值超出（1025~65535）范围！"), CONFIG_FILE);
		AfxMessageBox(err);
		return FALSE;
	}
	if (sDebugUI == _T("ON")) {
		m_bDebugUI = TRUE;
	}
	if (sDebugArea == _T("ON")) {
		m_bDebugArea = TRUE;
	}
	if (sDebugSocket == _T("ON")) {
		m_bDebugSocket = TRUE;
	}
	if (sDebugStorage == _T("ON")) {
		m_bDebugStorage = TRUE;
	}
	if (sDebugNoLogin == _T("ON")) {
		m_bDebugNoLogin = TRUE;
	}
	if (m_bDebugSocket && sDebugSocketDetail == _T("ON")) {
		m_bDebugSocketDetail = TRUE;
	}
	return TRUE;
}

BOOL CFMCenterApp::InitSystemSetup()
{
	// 执行查询
	CRecordset rs(&m_db);
	CString sql =
		_T("SELECT hospital_name,fhr_speed_option,nibp_unit_option,temp_unit_option,fhr_report_type,tend_print_setup ")
		_T("FROM system_setup ")
		_T("WHERE sid='0'");

	OutputDebugString(sql + _T("\n"));

	// 执行SQL语句
	CSmartLock lock(m_csDBLock);
	try {
		rs.Open(AFX_DB_USE_DEFAULT_TYPE, sql);
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CFMCenterApp::InitSystemSetup第1个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		return FALSE;
	}

	// 将查询内容写入列表
	int nCount;
	try {
		while (! rs.IsEOF()) {
			rs.MoveNext();
		}
		nCount = rs.GetRecordCount();
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CFMCenterApp::InitSystemSetup第2个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		return FALSE;
	}

	if (0 == nCount) {
		//说明未创建系统设置选项，写入默认值
		sql =
		_T("INSERT system_setup (sid,hospital_name,fhr_speed_option,nibp_unit_option,temp_unit_option,fhr_report_type,tend_print_setup) ")
		_T("VALUES ('0','','0','0','0','0','11111111');");
		try {
			m_db.ExecuteSQL(sql);
		}
		catch (CDBException* pe) {
			_TCHAR err[255];
			pe->GetErrorMessage(err, 255);
			theApp.WriteLog(_T("异常位于：CFMCenterApp::InitSystemSetup第3个catch\n"));
			theApp.WriteLog(err);
			theApp.WriteLog(_T("\n"));
			pe->Delete();
			return FALSE;
		}
		rs.Close();
		return FALSE;
	}

	try {
		rs.MoveFirst();
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CFMCenterApp::InitSystemSetup第4个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		return FALSE;
	}

	if (! rs.IsEOF()) {
		try {
			CString sValue;
			CDBVariant var;

			rs.GetFieldValue(_T("hospital_name"), sValue);
			m_sHospitalName = sValue;

			rs.GetFieldValue(_T("fhr_speed_option"), var);
			m_nFHRSpeedOption = var.m_lVal;

			rs.GetFieldValue(_T("nibp_unit_option"), var);
			m_nNIBPUnitOption = var.m_lVal;

			rs.GetFieldValue(_T("temp_unit_option"), var);
			m_nTEMPUnitOption = var.m_lVal;

			rs.GetFieldValue(_T("fhr_report_type"), var);
			m_nFHRReportType = var.m_lVal;

			rs.GetFieldValue(_T("tend_print_setup"), sValue);
			m_bTendPrintHR   = (sValue.GetAt(7) == _T('1'));
			m_bTendPrintSYS  = (sValue.GetAt(6) == _T('1'));
			m_bTendPrintMEA  = (sValue.GetAt(5) == _T('1'));
			m_bTendPrintDIA  = (sValue.GetAt(4) == _T('1'));
			m_bTendPrintRR   = (sValue.GetAt(3) == _T('1'));
			m_bTendPrintSPO2 = (sValue.GetAt(2) == _T('1'));
			m_bTendPrintT1   = (sValue.GetAt(1) == _T('1'));
			m_bTendPrintT2   = (sValue.GetAt(0) == _T('1'));
		}
		catch (CDBException* pe) {
			_TCHAR err[255];
			pe->GetErrorMessage(err, 255);
			theApp.WriteLog(_T("异常位于：CFMCenterApp::InitSystemSetup第5个catch\n"));
			theApp.WriteLog(err);
			theApp.WriteLog(_T("\n"));
			pe->Delete();
			return FALSE;
		}
	}

	if (rs.IsOpen()) {
		rs.Close();
	}
	return TRUE;
}

BOOL CFMCenterApp::SaveSystemSetup()
{
	CSmartLock lock(m_csDBLock);
	CString sTendPrintSetup;
	sTendPrintSetup.Format(_T("%d%d%d%d%d%d%d%d"),
		m_bTendPrintHR,
		m_bTendPrintSYS,
		m_bTendPrintMEA,
		m_bTendPrintDIA,
		m_bTendPrintRR,
		m_bTendPrintSPO2,
		m_bTendPrintT1,
		m_bTendPrintT2);

	CString sql;
	sql.Format(
		_T("UPDATE system_setup SET ")
		_T("hospital_name='%s',fhr_speed_option='%d',nibp_unit_option='%d',temp_unit_option='%d',fhr_report_type='%d',tend_print_setup='%s' ")
		_T("WHERE sid='0'"),
		m_sHospitalName, m_nFHRSpeedOption, m_nNIBPUnitOption, m_nTEMPUnitOption, m_nFHRReportType, sTendPrintSetup);

	try {
		m_db.ExecuteSQL(sql);
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CFMCenterApp::SaveSystemSetup\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		return FALSE;
	}

	return TRUE;
}

int CFMCenterApp::GetRecordFileList(CStringArray& sa, CString mrid,
		CUIntArray* paOffset, CUIntArray* paSize)
{
	CSmartLock lock(m_csDBLock);
	CRecordset rs(&m_db);
	CString sql;

	// 构造SQL语句
	sql.Format(
		_T("SELECT fname,foffset,fsize FROM file_record ")
		_T("WHERE mrid='%s'"),
		mrid);

	OutputDebugString(sql + _T("\n"));

	try {
		rs.Open(AFX_DB_USE_DEFAULT_TYPE, sql);
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CFMCenterApp::GetRecordFileList第1个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		return 0;
	}

	try {
		rs.MoveFirst();
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CFMCenterApp::GetRecordFileList第2个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		return 0;
	}

	while (! rs.IsEOF()) {
		try {
			CDBVariant var;
			CString sValue;
			CString sFullName;
			rs.GetFieldValue(_T("fname"), sValue);

			sFullName.Format(_T("%s%s/%s"), theApp.GetDataRoot(), RECORD_DIR, sValue);
			sa.Add(sFullName);

			if (paOffset) {
				rs.GetFieldValue(_T("foffset"), var);
				paOffset->Add((UINT)var.m_lVal);
			}

			if (paSize) {
				rs.GetFieldValue(_T("fsize"), var);
				paSize->Add((UINT)var.m_lVal);
			}
		}
		catch (CDBException* pe) {
			_TCHAR err[255];
			pe->GetErrorMessage(err, 255);
			theApp.WriteLog(_T("异常位于：CFMCenterApp::GetRecordFileList第3个catch\n"));
			theApp.WriteLog(err);
			theApp.WriteLog(_T("\n"));
			pe->Delete();
			goto fini;
		}
		rs.MoveNext();
	}

	if (rs.IsOpen()) {
		rs.Close();
	}

fini:

	return sa.GetCount();
}

CFMRecordUnit* CFMCenterApp::GetRecordUnit(int nIndex)
{
	ASSERT(nIndex >= 0 && nIndex < CLIENT_COUNT);
	if (m_bOK) {
		return m_pRecordThread->GetUnit(nIndex);
	}
	theApp.WriteLog(_T("！！！ CFMCenterApp::GetRecordUnit在m_bOK之前被调用！"));
	theApp.WriteLog(_T("\n"));
	return NULL;
}

void CFMCenterApp::PostSocketMessage()
{
	if (m_pMainWnd) {
		m_pMainWnd->PostMessage(WM_USER_SOCKET_CHANGE);
	}
}

void CFMCenterApp::PostLayoutMessage()
{
	if (m_pMonitorView) {
		m_pMonitorView->PostMessage(WM_USER_LAYOUT_CHANGE);
	}
}

void CFMCenterApp::PostInitBkMessage()
{
	if (m_pMainWnd) {
		m_pMainWnd->PostMessage(WM_USER_INIT_BK_FINISH);
	}
}

void CFMCenterApp::Close()
{
	if (m_pMonitorView) {
		m_pMonitorView->CloseTimer();
	}
	if (m_pToolbarForm) {
		m_pToolbarForm->CloseTimer();
	}
	if (m_pMainWnd) {
		((CMainFrame*)m_pMainWnd)->Close();
	}
}

int CFMCenterApp::GetDoctorDiagno(CString mrid)
{
	CSmartLock lock(m_csDBLock);
	CRecordset rs(&m_db);
	CString sql;
	int nRet = 0;

	// 构造SQL语句
	sql.Format(
		_T("SELECT doctor_diagno FROM monitor_record ")
		_T("WHERE mrid='%s'"),
		mrid);

	OutputDebugString(sql + _T("\n"));

	try {
		rs.Open(AFX_DB_USE_DEFAULT_TYPE, sql);
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CFMCenterApp::GetDoctorDiagno第1个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		return 0;
	}

	try {
		rs.MoveFirst();
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CFMCenterApp::GetDoctorDiagno第2个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		return 0;
	}

	if (! rs.IsEOF()) {
		try {
			CDBVariant var;
			rs.GetFieldValue(_T("doctor_diagno"), var);
			nRet = var.m_lVal;
		}
		catch (CDBException* pe) {
			_TCHAR err[255];
			pe->GetErrorMessage(err, 255);
			theApp.WriteLog(_T("异常位于：CFMCenterApp::GetDoctorDiagno第3个catch\n"));
			theApp.WriteLog(err);
			theApp.WriteLog(_T("\n"));
			pe->Delete();
			return 0;
		}
		rs.MoveNext();
	}

	if (rs.IsOpen()) {
		rs.Close();
	}

	return nRet;
}

void CFMCenterApp::SaveDoctorDiagno(CString mrid, int n)
{
	CSmartLock lock(m_csDBLock);
	CRecordset rs(&m_db);
	CString sql;
	int nRet = 0;

	// 构造SQL语句
	sql.Format(
		_T("UPDATE monitor_record SET doctor_diagno='%d',operator='%s' ")
		_T("WHERE mrid='%s'"),
		n, theApp.m_sAdminName, mrid);

	OutputDebugString(sql + _T("\n"));

	//执行SQL语句
	try {
		m_db.ExecuteSQL(sql);
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CFMCenterApp::SaveDoctorDiagno第1个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		AfxMessageBox(_T("数据库操作失败！"), MB_ICONERROR | MB_OK);
	}
}
// CFMCenterApp 消息处理程序


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()

// 用于运行对话框的应用程序命令
void CFMCenterApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

BOOL CFMCenterApp::OnIdle(LONG lCount)
{
	// 检查Socket队列中有没有失效的SocketUnit
	if (m_pSocketThread) {
		m_pSocketThread->CheckSockets();
	}
	return CWinApp::OnIdle(lCount);
}
