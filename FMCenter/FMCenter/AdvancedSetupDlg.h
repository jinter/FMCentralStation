#pragma once
#include "afxwin.h"
#include "afxcmn.h"


// CAdvancedSetupDlg 对话框

class CAdvancedSetupDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CAdvancedSetupDlg)

public:
	CAdvancedSetupDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CAdvancedSetupDlg();

// 对话框数据
	enum { IDD = IDD_ADVANCED_SETUP };

private:
	CDatabase& m_db;
	CCriticalSection& m_dbLock;
	int m_nListCurrentIndex;

	void UpdateAdminList();
	void SetButtonState();
	void AddNewAdmin(CString& sName, CString& sPass, int nType);
	void UpdateAdmin(int nID, CString& sName, CString& sPass, int nType);
	void DelAdmin(int nID);
	BOOL CheckAdminInfo(CString& sName, CString& sPass1, CString& sPass2);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBtnStartSim();
	afx_msg void OnBnClickedBtnAddAdmin();
	afx_msg void OnBnClickedBtnEditAdmin();
	afx_msg void OnBnClickedBtnDelAdmin();
	afx_msg void OnBnClickedOk();
	afx_msg void OnItemclickLstAdmin(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedBtnSaveSystemSetup();
	CEdit m_edtHospitalName;
	CListCtrl m_lstAdminList;
	CEdit m_edtAdminName;
	CButton m_chkAdminType;
	CButton m_btnAddAdmin;
	CButton m_btnEditAdmin;
	CButton m_btnDelAdmin;
	CEdit m_edtPass1;
	CEdit m_edtPass2;
	CComboBox m_cmbFHRPaperSpeed;
	CComboBox m_cmbNIBPUnit;
	CComboBox m_cmbTEMPUnit;
	CButton m_chkTendPrintHR;
	CButton m_chkTendPrintSYS;
	CButton m_chkTendPrintMEA;
	CButton m_chkTendPrintDIA;
	CButton m_chkTendPrintRR;
	CButton m_chkTendPrintSPO2;
	CButton m_chkTendPrintT1;
	CButton m_chkTendPrintT2;
	CComboBox m_cmbFHRReportType;
};
