#include "stdafx.h"
#include "LogFile.h"


CLogFile::CLogFile(CString sFileName)
{
	if (! m_fileLog.Open(sFileName, CFile::modeCreate | CFile::modeWrite)) {
		CString err;
		err.Format(_T("日志文件创建失败！\n\n请去除%s所在目录的只读属性！\n"), sFileName);
		AfxMessageBox(err);
		exit(0);
	}
	if (2 == sizeof(_TCHAR)) {
		BYTE head[2] = { 0xFF, 0xFE };//UNICODE格式的，写文件头
		m_fileLog.Write(head, 2);
	}
	m_nMsgCounter = 0;
}

CLogFile::~CLogFile(void)
{
	m_fileLog.Flush();
	m_fileLog.Close();
}

void CLogFile::Write(CString s)
{
	s.Replace(_T("\n"), _T("\r\n"));
	int len = s.GetLength();
	if (2 == sizeof(_TCHAR)) {
		m_fileLog.Write(s.GetBuffer(0), len*2);//UNICODE是双字节的啊
	}
	else {
		m_fileLog.Write(s.GetBuffer(0), len);
	}
	m_nMsgCounter ++;
	if (m_nMsgCounter > m_nSaveCount) {
		m_fileLog.Flush();
		m_nMsgCounter = 0;
	}
}
