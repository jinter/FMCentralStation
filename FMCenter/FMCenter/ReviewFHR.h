#pragma once
#include "reviewdlg.h"

class CFHRAutodiagnostics;
class CReviewFHR : public CReviewTool
{
private:
	CFont m_fontTimeScale;
	CFont m_fontTableScale;
	CFont* m_pOldFont;
	CFHRAutodiagnostics* m_pAuto1; //两路波形的自动诊断
	CFHRAutodiagnostics* m_pAuto2;
	CLIENT_DATA* m_pOldDataPtr;

private:
	void SelectTimeRulerFont(CDC* pdc);
	void SelectTableScaleFont(CDC* pdc);
	void ResetFont(CDC* pdc);
	void Autodiagnostics(CLIENT_DATA* pData, int nCount);

public:
	CReviewFHR(CReviewDlg* pdlg);
	virtual ~CReviewFHR(void);

private:
	virtual int  GetPageSpan();
	virtual void EnterReview();
	virtual void LeaveReview();
	virtual void Draw(CDC* pdc);

private:
	void DrawGrid(CDC* pdc, CRect& rc);
	void DrawTime(CDC* pdc, CRect& rc);
	void DrawWave(CDC* pdc, CRect& rc);
	void DrawDiagnosisFlag(CDC* pdc, COLORREF cr, int x, int y, int type);
};

