// SetupSPO2Dlg.cpp : 实现文件
//

#include "stdafx.h"
#include "FMCenter.h"
#include "SetupSPO2Dlg.h"
#include "afxdialogex.h"
#include "FMSocketThread.h"
#include "FMSocketUnit.h"
#include "FMRecordUnit.h"

// CSetupSPO2Dlg 对话框

IMPLEMENT_DYNAMIC(CSetupSPO2Dlg, CDialogEx)

CSetupSPO2Dlg::CSetupSPO2Dlg(CWnd* pParent, CFMRecordUnit* pru)
	: CDialogEx(CSetupSPO2Dlg::IDD, pParent)
	, m_pru(pru)
{
}

CSetupSPO2Dlg::~CSetupSPO2Dlg()
{
}

void CSetupSPO2Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_SPO2_HIGH, m_edtSPO2High);
	DDX_Control(pDX, IDC_EDIT_SPO2_LOW, m_edtSPO2Low);
	DDX_Control(pDX, IDC_EDIT_PR_HIGH, m_edtPRHigh);
	DDX_Control(pDX, IDC_EDIT_PR_LOW, m_edtPRLow);
}

BEGIN_MESSAGE_MAP(CSetupSPO2Dlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CSetupSPO2Dlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CSetupSPO2Dlg::OnBnClickedCancel)
END_MESSAGE_MAP()

// CSetupSPO2Dlg 消息处理程序
void CSetupSPO2Dlg::OnBnClickedOk()
{
	// 发送设置
	CString sValue;
	m_edtSPO2High.GetWindowText(sValue);
	int spo2_h = _ttoi(sValue);
	m_edtSPO2Low.GetWindowText(sValue);
	int spo2_l = _ttoi(sValue);
	m_edtPRHigh.GetWindowText(sValue);
	int pr_h = _ttoi(sValue);
	m_edtPRLow.GetWindowText(sValue);
	int pr_l = _ttoi(sValue);

	if (spo2_h < 0 || spo2_h > 100) {
		MessageBox(_T("血氧上限超出范围！"));
		m_edtSPO2High.SetFocus();
		return;
	}
	if (spo2_l < 0 || spo2_l > 100) {
		MessageBox(_T("血氧下限超出范围！"));
		m_edtSPO2Low.SetFocus();
		return;
	}
	if (spo2_l >= spo2_h) {
		MessageBox(_T("血氧下限不能超越上限设定！"));
		m_edtSPO2Low.SetFocus();
		return;
	}
	if (pr_h < 0 || pr_h > 254) {
		MessageBox(_T("脉率上限超出范围！"));
		m_edtPRHigh.SetFocus();
		return;
	}
	if (pr_l < 0 || pr_l > 254) {
		MessageBox(_T("脉率下限超出范围！"));
		m_edtPRLow.SetFocus();
		return;
	}
	if (pr_l >= pr_h) {
		MessageBox(_T("脉率下限不能超越上限设定！"));
		m_edtPRLow.SetFocus();
		return;
	}

	CFMSocketUnit* psu = theApp.m_pSocketThread->GetUnit(m_pru->GetIndex());
	psu->SendSetupSPO2(spo2_h, spo2_l, pr_h, pr_l);
}

void CSetupSPO2Dlg::OnBnClickedCancel()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialogEx::OnCancel();
}

BOOL CSetupSPO2Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	DISPLAY_DATA* pdd = m_pru->GetLatestNumbData();
	if (pdd) {
		CString sValue;
		sValue.Format(_T("%d"), pdd->spo2_h);
		m_edtSPO2High.SetWindowText(sValue);
		sValue.Format(_T("%d"), pdd->spo2_l);
		m_edtSPO2Low.SetWindowText(sValue);
		sValue.Format(_T("%d"), pdd->pr_h);
		m_edtPRHigh.SetWindowText(sValue);
		sValue.Format(_T("%d"), pdd->pr_l);
		m_edtPRLow.SetWindowText(sValue);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}
