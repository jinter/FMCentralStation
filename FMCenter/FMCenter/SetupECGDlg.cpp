// SetupECGDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "FMCenter.h"
#include "SetupECGDlg.h"
#include "afxdialogex.h"
#include "FMSocketThread.h"
#include "FMSocketUnit.h"
#include "FMRecordUnit.h"


// CSetupECGDlg 对话框

IMPLEMENT_DYNAMIC(CSetupECGDlg, CDialogEx)

CSetupECGDlg::CSetupECGDlg(CWnd* pParent, CFMRecordUnit* pru)
	: CDialogEx(CSetupECGDlg::IDD, pParent)
	, m_pru(pru)
{
}

CSetupECGDlg::~CSetupECGDlg()
{
}

void CSetupECGDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_ECG1_LEAD, m_cmbECG1Lead);
	DDX_Control(pDX, IDC_COMBO_ECG2_LEAD, m_cmbECG2Lead);
	DDX_Control(pDX, IDC_COMBO_ECG_FILTER, m_cmbECGFilter);
	DDX_Control(pDX, IDC_COMBO_ECG_GAIN, m_cmbECGGain);
	DDX_Control(pDX, IDC_COMBO_ECG_STOP, m_cmbECGStop);
	DDX_Control(pDX, IDC_COMBO_ECG_PACEANGY, m_cmbECGPaceangy);
	DDX_Control(pDX, IDC_COMBO_ECG_STANGY, m_cmbECGStangy);
	DDX_Control(pDX, IDC_COMBO_ECG_RHYTHM, m_cmbECGRhythm);
	DDX_Control(pDX, IDC_EDIT_HR_HIGH, m_edtHRHigh);
	DDX_Control(pDX, IDC_EDIT_HR_LOW, m_edtHRLow);
	DDX_Control(pDX, IDC_EDIT_ECG1_ST_HIGH, m_edtECG1StHigh);
	DDX_Control(pDX, IDC_EDIT_ECG1_ST_LOW, m_edtECG1StLow);
	DDX_Control(pDX, IDC_EDIT_ECG2_ST_HIGH, m_edtECG2StHigh);
	DDX_Control(pDX, IDC_EDIT_ECG2_ST_LOW, m_edtECG2StLow);
	DDX_Control(pDX, IDC_EDIT_ECG_PVC_HIGH, m_edtPVCHigh);
}

BEGIN_MESSAGE_MAP(CSetupECGDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CSetupECGDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CSetupECGDlg::OnBnClickedCancel)
	ON_EN_UPDATE(IDC_EDIT_ECG1_ST_HIGH, &CSetupECGDlg::OnUpdateEditEcg1StHigh)
	ON_EN_UPDATE(IDC_EDIT_ECG1_ST_LOW, &CSetupECGDlg::OnUpdateEditEcg1StLow)
END_MESSAGE_MAP()

// CSetupECGDlg 消息处理程序
BOOL CSetupECGDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_cmbECG1Lead.ResetContent();
	m_cmbECG2Lead.ResetContent();
	m_cmbECGFilter.ResetContent();
	m_cmbECGGain.ResetContent();
	m_cmbECGStop.ResetContent();
	m_cmbECGPaceangy.ResetContent();
	m_cmbECGStangy.ResetContent();
	m_cmbECGRhythm.ResetContent();
	int i;
	int count = CFMDict::GetECGLeadCount();
	for (i=0; i<count; i++) {
		m_cmbECG1Lead.AddString(CFMDict::ECGLead_itos(i));
		m_cmbECG2Lead.AddString(CFMDict::ECGLead_itos(i));
	}
	count = CFMDict::GetECGFilterCount();
	for (i=0; i<count; i++) {
		m_cmbECGFilter.AddString(CFMDict::ECGFilter_itos(i));
	}
	count = CFMDict::GetECGGainCount();
	for (i=0; i<count; i++) {
		m_cmbECGGain.AddString(CFMDict::ECGGain_itos(i));
	}
	count = CFMDict::GetOnOffCount();
	for (i=0; i<count; i++) {
		m_cmbECGStop.AddString(CFMDict::OnOff_itos(i));
		m_cmbECGPaceangy.AddString(CFMDict::OnOff_itos(i));
		m_cmbECGStangy.AddString(CFMDict::OnOff_itos(i));
		m_cmbECGRhythm.AddString(CFMDict::OnOff_itos(i));
	}

	DISPLAY_DATA* pdd = m_pru->GetLatestNumbData();
	if (pdd) {
		m_cmbECG1Lead.SetCurSel(pdd->ecg1_lead);
		m_cmbECG2Lead.SetCurSel(pdd->ecg2_lead);
		m_cmbECGFilter.SetCurSel(pdd->ecg_filter);
		m_cmbECGGain.SetCurSel(pdd->ecg_gain);
		m_cmbECGStop.SetCurSel(pdd->ecg_stop);
		m_cmbECGPaceangy.SetCurSel(pdd->ecg_paceangy);
		m_cmbECGStangy.SetCurSel(pdd->ecg_stangy);
		m_cmbECGRhythm.SetCurSel(pdd->ecg_rhythm);
		CString sValue;
		sValue.Format(_T("%d"), pdd->hr_h);
		m_edtHRHigh.SetWindowText(sValue);
		sValue.Format(_T("%d"), pdd->hr_l);
		m_edtHRLow.SetWindowText(sValue);
		sValue.Format(_T("%.2f"), (0.01f * (float)(short)pdd->st1_h));
		m_edtECG1StHigh.SetWindowText(sValue);
		m_edtECG2StHigh.SetWindowText(sValue);
		sValue.Format(_T("%.2f"), (0.01f * (float)(short)pdd->st1_l));
		m_edtECG1StLow.SetWindowText(sValue);
		m_edtECG2StLow.SetWindowText(sValue);
		//sValue.Format(_T("%.2f"), (0.01f * (float)(short)pdd->st2_h));
		//sValue.Format(_T("%.2f"), (0.01f * (float)(short)pdd->st2_l));
		sValue.Format(_T("%d"), pdd->pvc_h);
		m_edtPVCHigh.SetWindowText(sValue);
	}

	
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CSetupECGDlg::OnBnClickedOk()
{
	// 发送设置
	int ecg_l1 = m_cmbECG1Lead.GetCurSel();
	int ecg_l2 = m_cmbECG2Lead.GetCurSel();
	int filter = m_cmbECGFilter.GetCurSel();
	int gain = m_cmbECGGain.GetCurSel();
	int stop = m_cmbECGStop.GetCurSel();
	int paceangy = m_cmbECGPaceangy.GetCurSel();
	int stangy = m_cmbECGStangy.GetCurSel();
	int rhythm = m_cmbECGRhythm.GetCurSel();
	CString sValue;
	m_edtHRHigh.GetWindowText(sValue);
	int hr_h = _ttoi(sValue);
	m_edtHRLow.GetWindowText(sValue);
	int hr_l = _ttoi(sValue);
	m_edtECG1StHigh.GetWindowText(sValue);
	int st1_h = (int)(_ttof(sValue) * 100);
	m_edtECG1StLow.GetWindowText(sValue);
	int st1_l = (int)(_ttof(sValue) * 100);
	//m_edtECG2StHigh.GetWindowText(sValue);
	//int st2_h = (int)(_ttof(sValue) * 100);
	//m_edtECG2StLow.GetWindowText(sValue);
	//int st2_l = (int)(_ttof(sValue) * 100);
	int st2_h = st1_h;
	int st2_l = st1_l;
	m_edtPVCHigh.GetWindowText(sValue);
	int pvc_h = _ttoi(sValue);

	if (hr_h < 15 || hr_h > 300) {
		MessageBox(_T("心率上限超出范围！"));
		m_edtHRHigh.SetFocus();
		return;
	}
	if (hr_l < 15 || hr_l > 300) {
		MessageBox(_T("心率下限超出范围！"));
		m_edtHRLow.SetFocus();
		return;
	}
	if (hr_l >= hr_h) {
		MessageBox(_T("心率下限不能超越上限设定！"));
		m_edtHRLow.SetFocus();
		return;
	}
	if (st1_h < -200 || st1_h > 200) {
		MessageBox(_T("1通道ST上限超出范围！"));
		m_edtECG1StHigh.SetFocus();
		return;
	}
	if (st1_l < -200 || st1_l > 200) {
		MessageBox(_T("1通道ST下限超出范围！"));
		m_edtECG1StLow.SetFocus();
		return;
	}
	if (st1_l >= st1_h) {
		MessageBox(_T("1通道ST下限不能超越上限设定！"));
		m_edtECG1StLow.SetFocus();
		return;
	}
	//if (st2_h < -200 || st2_h > 200) {
	//	MessageBox(_T("2通道ST上限超出范围！"));
	//	m_edtECG1StHigh.SetFocus();
	//	return;
	//}
	//if (st2_l < -200 || st2_l > 200) {
	//	MessageBox(_T("2通道ST下限超出范围！"));
	//	m_edtECG1StLow.SetFocus();
	//	return;
	//}
	//if (st2_l >= st2_h) {
	//	MessageBox(_T("2通道ST下限不能超越上限设定！"));
	//	m_edtECG1StLow.SetFocus();
	//	return;
	//}
	if (pvc_h < 0 || pvc_h > 100) {
		MessageBox(_T("PVC报警上限超出范围！"));
		m_edtPVCHigh.SetFocus();
		return;
	}

	CFMSocketUnit* psu = theApp.m_pSocketThread->GetUnit(m_pru->GetIndex());
	psu->SendSetupECG(ecg_l1, ecg_l2, filter, gain, stop, paceangy, stangy, rhythm,
		hr_h, hr_l, st1_h, st1_l, st2_h, st2_l, pvc_h, 0);
}

void CSetupECGDlg::OnBnClickedCancel()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialogEx::OnCancel();
}

void CSetupECGDlg::OnUpdateEditEcg1StHigh()
{
	// TODO:  如果该控件是 RICHEDIT 控件，它将不
	// 发送此通知，除非重写 CDialogEx::OnInitDialog()
	// 函数，以将 EM_SETEVENTMASK 消息发送到该控件，
	// 同时将 ENM_UPDATE 标志“或”运算到 lParam 掩码中。

	CString sValue;
	m_edtECG1StHigh.GetWindowText(sValue);
	m_edtECG2StHigh.SetWindowText(sValue);
}


void CSetupECGDlg::OnUpdateEditEcg1StLow()
{
	// TODO:  如果该控件是 RICHEDIT 控件，它将不
	// 发送此通知，除非重写 CDialogEx::OnInitDialog()
	// 函数，以将 EM_SETEVENTMASK 消息发送到该控件，
	// 同时将 ENM_UPDATE 标志“或”运算到 lParam 掩码中。

	CString sValue;
	m_edtECG1StLow.GetWindowText(sValue);
	m_edtECG2StLow.SetWindowText(sValue);
}
