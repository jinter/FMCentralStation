
// MainFrm.cpp : CMainFrame 类的实现
//

#include "stdafx.h"
#include "FMCenter.h"
#include "SplashWnd.h"
#include "MainFrm.h"
#include "FMMonitorView.h"
#include "FMToolbarForm.h"
#include "FMSocketThread.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNCREATE(CFMSplitterWnd, CSplitterWnd)

BEGIN_MESSAGE_MAP(CFMSplitterWnd, CSplitterWnd)
ON_WM_NCHITTEST()
END_MESSAGE_MAP()

BOOL CFMSplitterWnd::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_VSCROLL & ~WS_HSCROLL & ~WS_THICKFRAME;
	return CSplitterWnd::PreCreateWindow(cs);
}

// CMainFrame
IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_NCHITTEST()
	ON_MESSAGE(WM_USER_SOCKET_CHANGE, &CMainFrame::OnSocketChange)
	ON_MESSAGE(WM_USER_INIT_BK_FINISH, &CMainFrame::OnInitBkFinish)
	ON_WM_NCHITTEST()
	ON_WM_SYSCOMMAND()
	ON_WM_CLOSE()
END_MESSAGE_MAP()

// CMainFrame 构造/析构
CMainFrame::CMainFrame()
{
}

CMainFrame::~CMainFrame()
{
}

BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT /*lpcs*/,
	CCreateContext* pContext)
{
	//return m_wndSplitter.Create(this,
	//	2, 2,               // TODO: 调整行数和列数
	//	CSize(10, 10),      // TODO: 调整最小窗格大小
	//	pContext);

	//将窗口分为两行一列
	DWORD dwStyle = WS_CHILD | WS_VISIBLE;
	if (!m_wndSplitter.CreateStatic(this, 2, 1, dwStyle)) {
		return FALSE;
	}

	//指定每个窗口的位置及初始大小
	if (!m_wndSplitter.CreateView(0, 0, RUNTIME_CLASS(CFMMonitorView), CSize(MAIN_AREA_W, MAIN_AREA_H), pContext) ||
		!m_wndSplitter.CreateView(1, 0, RUNTIME_CLASS(CFMToolbarForm), CSize(MAIN_TOOLBAR_W, MAIN_TOOLBAR_H), pContext))
	{
		m_wndSplitter.DestroyWindow();
		return FALSE;
	}
	return TRUE;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	cs.style = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU;// | WS_EX_TOPMOST;

	cs.hMenu = NULL;

	cs.cx = CLIENT_W + 10;
	cs.cy = CLIENT_H + 40;
	cs.x = -5;
	cs.y = -5;
	m_strTitle = MAIN_WINDOW_TITLE;
	return TRUE;
}

// CMainFrame 诊断
#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}
#endif //_DEBUG

// CMainFrame 消息处理程序
int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1) {
		return -1;
	}
	::SetProp(this->m_hWnd, ::AfxGetAppName(), (HANDLE)1);

	//注意：下面这条语句会把调试器挡住。调试时可暂时屏蔽。
	//设置窗口为置顶窗口
	//SetWindowPos(&wndTopMost, 0,0,0,0, SWP_NOMOVE | SWP_NOSIZE);

	//显示LOGO窗口
	CSplashWnd::ShowSplashScreen(this);
	return 0;
}

void CMainFrame::OnDestroy()
{
	CFrameWnd::OnDestroy();
	::RemoveProp(this->m_hWnd, ::AfxGetAppName());
}

LRESULT CMainFrame::OnSocketChange(WPARAM wp, LPARAM lp)
{
	//收到此事件，表示有新的床边机进入监护状态；
	//或者有床边机退出监护状态。
	theApp.m_pToolbarForm->SetBedButton();
	return 0L;
}

LRESULT CMainFrame::OnInitBkFinish(WPARAM wp, LPARAM lp)
{
	//由于在Win7下不能锁定窗口，用户可能在初始化过程中移动了窗口。
	//当初始化完成之后，重新移动窗口位置。
	MoveWindow(-5, -5, CLIENT_W + 10, CLIENT_H + 40);
	return 0L;
}

LRESULT CMainFrame::OnNcHitTest(CPoint point)
{
	UINT nFlags = CMDIFrameWnd::OnNcHitTest(point);
	//禁止拖动窗口
	switch (nFlags) {
	case HTTOP:
	case HTTOPLEFT:
	case HTTOPRIGHT:
	case HTBOTTOM:
	case HTBOTTOMLEFT:
	case HTBOTTOMRIGHT:
	case HTLEFT:
	case HTRIGHT:
	case HTCAPTION:
	case HTSYSMENU:
		return HTCLIENT;
	}
	return nFlags;
}

void CMainFrame::OnSysCommand(UINT nID, LPARAM lParam)
{
	//禁止移动窗口
	switch (nID) {
	case SC_MOVE:
	case 0xF012:
		return;
	}

	CMDIFrameWnd::OnSysCommand(nID, lParam);
}

static DWORD WINAPI QuitFunction(LPVOID lpParam)
{
	//等待3秒之后强制关闭程序
	//如果退出之前的善后处理时间不够，可适当延长此时间
	Sleep(3000);
	ExitProcess(0);
}

void CMainFrame::Close()
{
	PostQuitMessage(WM_CLOSE);
	CreateThread(NULL, 4096, (LPTHREAD_START_ROUTINE)&QuitFunction, this, 0, 0);
}

void CMainFrame::OnClose()
{
	if (! theApp.m_pToolbarForm->QueryQuit()) {
		return;
	}

	theApp.Close();
}
