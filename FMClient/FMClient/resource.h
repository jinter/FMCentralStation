//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 FMClient.rc 使用
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     130
#define IDD_FMCLIENT                    130
#define IDC_CHK_ECG                     1003
#define IDC_CHK_SPO2                    1004
#define IDC_CHK_NIBP                    1005
#define IDC_CHK_TEMP                    1006
#define IDC_CHK_FETUS                   1007
#define IDC_CHK_IBP                     1008
#define IDC_CHK_CO2                     1009
#define IDC_CHK_REPRESS_MSGBOX          1010
#define ID_BTN_START_ALL                1012
#define IDC_S_INFLATE2                  1013
#define IDC_EDIT1                       1018
#define IDC_ERROR_INFO                  1018
#define ID_BTN_STOP_ALL                 1022
#define ID_BTN_SELECT_ALL               1023
#define ID_BTN_CLEAR_ALL                1024

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1019
#define _APS_NEXT_SYMED_VALUE           105
#endif
#endif
